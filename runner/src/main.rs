fn main() {
    println!("Hello, world!");

    unsafe {
        // Load the evil DLL. This would be done with some other technique.
        let lib = libloading::Library::new(r"target\debug\wrapper.dll").unwrap();

        // Call the first exported function. This one should be wrapped with evil code.
        let func: libloading::Symbol<unsafe extern "C" fn()> = lib.get(b"rust_function_a").unwrap();
        func();

        // Call the second exported function from the original DLL.
        let func: libloading::Symbol<unsafe extern "C" fn()> = lib.get(b"rust_function_b").unwrap();
        func();
    }
}
