How to run demo:
```
cd demo
cargo build
cd ../wrapper
cargo build
cd ..
cargo run
```
The output should be
```
Hello, world!
evil rust function a before
rust function a
evil rust function a after
rust function b
```