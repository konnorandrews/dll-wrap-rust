#[no_mangle]
pub extern "C" fn rust_function_a() {
    println!("evil rust function a before");

    // Manually load the original DLL to use the original definition of the function.
    unsafe {
        let lib =
            libloading::Library::new(r"target\debug\demo.dll")
                .unwrap();
        let func: libloading::Symbol<unsafe extern "C" fn()> = lib.get(b"rust_function_a").unwrap();
        func();
    }

    println!("evil rust function a after");
}

// Tell rustc we want to link to a library called "other", which is what we named it in the build script.
#[link(name = "other")]
extern "C" {}
