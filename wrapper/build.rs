fn main() {
    // Tell the linker where the original DLL is.
    println!(
        r"cargo:rustc-link-lib=dylib=other:target\debug\demo.dll"
    );

    // Tell the linker to export the symbol from the original DLL for the B variant.
    println!(
        r"cargo:rustc-link-arg=/export:rust_function_b=target\debug\demo.rust_function_b"
    );
}
