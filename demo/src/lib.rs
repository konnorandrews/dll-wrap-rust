// These two functions are our external symbols for the DLL. We will attempt to overwrite the a variant.

#[no_mangle]
pub extern "C" fn rust_function_a() {
    println!("rust function a");
}

#[no_mangle]
pub extern "C" fn rust_function_b() {
    println!("rust function b");
}
